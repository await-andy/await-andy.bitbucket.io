addEventListener('fetch', event => {
	const { method, url } = event.request;
	if ('GET' !== method) return;
	if (url.includes('sw-log')) {
		const body = JSON.stringify({ logs });
		event.respondWith(new Response(body));
	} else {
		log(url);
	}
	// const url = new URL(event.request.url);
	// if (excludePaths.includes(url.pathname)) return false;
	// event.respondWith(fetch(url.href, event.request));
});

addEventListener('message', ({ source, data }) => {
	source.postMessage('echo> ' + data);
});

addEventListener('activate', event => {
	log('activate');
	const { clients } = self;
	if (clients && clients.claim) {
		log('claim');
		const p = clients.claim().then(() => log('claimed'));
		event.waitUntil(p);
	}
});

addEventListener('install', () => {
	const { skipWaiting, clients } = self;
	if (skipWaiting && clients && clients.claim) {
		skipWaiting();
	}
});

const logs = [];

function log(msg) {
	logs.push(msg)
}
