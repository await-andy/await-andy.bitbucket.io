const dom = {
	statuses: document.getElementById('statuses'),
};

const urls = getUrls();
if (urls.length) {
	urls
		.map(() => document.createElement('div'))
		.forEach((e, i) => {
			dom['status' + i] = e;
			dom.statuses.appendChild(e);
		});
	getStatuses(urls);
}

function getStatuses(urls) {
	setTimeout(getStatuses, 60 * 1000, urls);
	urls.forEach((u, i) => fetch(u + '/online')
		.then(r => r.json())
		.then(r => status(u, r))
		.then(() => fetch(u + '/rpm')
			.then(r => r.json())
			.then(rpm => dom['status' + i].append(table(Object.entries(rpm))))
			.catch(console.log)
		)
		.catch(() => dom['status' + i].textContent = `${u} -`));
}

function getUrls() {
	const urls = localStorage.getItem('status-urls') || '[]';
	try {
		return JSON.parse(urls);
	} catch {
		return [];
	}
}

function status(url, s) {
	const i = urls.findIndex(u => u === url);
	const el = dom['status' + i];
	const n = (num, className) => `<span class="${className}">${num}</span>`;
	el.innerHTML = `<a href='${url}/channels' target="_blank">${url}</a>` +
		` ${n(s.Version, 'v')} ${n(s.Online, 'green')} ${n(s.Rps, 'num')} ${s.Uptime}`;
}

function table(content) {
	const t = document.createElement('table');
	content.forEach(r => {
		const tr = document.createElement('tr');
		tr.append(td(r[0]), td(r[1]));
		t.append(tr);
	});
	return t;
}

function td(text) {
	const td = document.createElement('td');
	td.textContent = text;
	return td;
}